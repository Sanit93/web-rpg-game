﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebRpgGame.Models;
using WebRpgGame.Models.MongoDb;

namespace WebRpgGame.Services.Secure
{
    public class Register
    {
        private Register() { }

        public static bool IsExist(RegisterData registerData)
        {
            var users = Repository.LoadRecords<Character>("Users");
            foreach(Character user in users)
            {
                if (registerData.AuthorizationData.Email.Equals(user.Email) || registerData.Name.Equals(user.Name))
                {
                    return true;
                }
            }
            return false;
        }


        public static void User(RegisterData registerData)
        {
            Secure.GetSHA1(registerData.AuthorizationData.Email, registerData.AuthorizationData.Password);

            var Character = new Character
            {
                Name = registerData.Name,
                Strenght = 0,
                Agility = 0,
                Inteligence = 0,
                Endurance = 0,
                Luck = 0,
                SHA1 = Secure.GetSHA1(registerData.AuthorizationData.Email, registerData.AuthorizationData.Password),
                Email = registerData.AuthorizationData.Email
            };

            Repository.InsertOneRecord("Users", Character);
        }
    }
}
