﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebRpgGame.Models;
using WebRpgGame.Models.MongoDb;
using WebRpgGame.Models.Users;

namespace WebRpgGame.Services.Secure
{
    public class Login
    {
        public static bool IsMatch(AuthorizationData data)
        {
            var p1 = Secure.GetSHA1(data.Email, data.Password);
            var users = Repository.LoadRecords<Character>("Users");

            if (IsAuthDataMatchWithUsers(p1, users)) return true;

            return false;
        }

        private static bool IsAuthDataMatchWithUsers(byte[] p1, List<Character> users)
        {
            foreach (Character character in users)
            {
                if (Secure.MatchSHA1(p1, character.SHA1) == true)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
