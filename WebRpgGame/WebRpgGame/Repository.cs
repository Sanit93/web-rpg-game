﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebRpgGame.Models.MongoDb
{
    public sealed class Repository
    {
        private Repository() { }

        private static IMongoDatabase DB { get; set; }

        public static void Initialize()
        {
            var client = new MongoClient();
            DB = client.GetDatabase("MyDataBase");
        }

        public static List<T> LoadRecords<T>(string table)
        {
            var collection = DB.GetCollection<T>(table);
            return collection.Find(new BsonDocument()).ToList();
        }

        public static void InsertOneRecord<T>(string table, T record)
        {
            var collection = DB.GetCollection<T>(table);
            collection.InsertOne(record);
        }

        public static T LoadRecordByID<T>(string table, Guid id)
        {
            var collection = DB.GetCollection<T>(table);
            var filter = Builders<T>.Filter.Eq("Id", id);

            return collection.Find(filter).First();
        }

        public static T LoadCharacterByName<T>(string name)
        {
            var collection = DB.GetCollection<T>("Users");
            var filter = Builders<T>.Filter.Eq("Name", name);

            return collection.Find(filter).First();
        }

        public static void UpdateRecord<T>(string table, Guid id, T record)
        {
            var collection = DB.GetCollection<T>(table);

            var result = collection.ReplaceOne(new BsonDocument("_id", id), record);
        }
    }
}
