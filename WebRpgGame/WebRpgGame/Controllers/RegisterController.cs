﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebRpgGame.Models;
using WebRpgGame.Models.MongoDb;
using WebRpgGame.Models.Users;
using WebRpgGame.Services.Secure;

namespace WebRpgGame.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        // GET: api/Register
        public ActionResult Get(AuthorizationData data)
        {
            if (Login.IsMatch(data))
            {
                return Ok();
            }
            return Unauthorized();
        }

        // POST: api/Register
        [HttpPost]
        public ActionResult Post([FromBody] RegisterData registerData)
        {
            if (Register.IsExist(registerData))
            {
                return Conflict();
            }
            else
            {
                Register.User(registerData);
                return Ok();
            }
        }
    }
}
