﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebRpgGame.Models;
using WebRpgGame.Models.Items.Weapons;
using WebRpgGame.Models.MongoDb;

namespace WebRpgGame.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class CharacterController : ControllerBase
    {
        // PUT: api/Character/EquipWeapon
        [Route("EquipWeapon")]
        [HttpPut]
        public ActionResult EquipWeapon(string name, [FromBody] Weapon weapon)
        {
            var character = Repository.LoadCharacterByName<Character>(name);
            character.EquipWeapon(weapon);

            return Ok();
        }

        // PUT: api/Character/IncraseStat
        [Route("IncraseStat")]
        [HttpPut]
        public ActionResult IncraseStat(string name, int stat)
        {
            var character = Repository.LoadCharacterByName<Character>(name);
            character.IncraseStat(stat);

            return Ok();
        }
    }
}