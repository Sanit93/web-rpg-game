﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using WebRpgGame.Models.Items;

namespace WebRpgGame.Models.Equipment
{
    public class Item
    {
        [BsonId]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Slots Slot { get; set; }
    }
}
