﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebRpgGame.Models.Items
{
    public enum Slots
    {
        None=0,Head=1, Chest=2, Legs=3, Hands=4, Feet=5, LeftHand=6, RightHand=7, TwoHands=8
    }
}
