﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebRpgGame.Models.Equipment;
using WebRpgGame.Models.MongoDb;

namespace WebRpgGame.Models.Items.Weapons
{
    public class Weapon:Item
    {
        public int Damage { get; set; }
        public TypesOfWeapons Type { get; set; }

        public Weapon(int damage, TypesOfWeapons type)
        {
            Damage = damage;
            Type = type;
        }
    }
}
