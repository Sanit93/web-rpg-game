﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebRpgGame.Models.Items.Weapons
{
    public enum TypesOfWeapons
    {
        Sword=1, Axe=2, Mace=3, Dagger=4, TwoHanded=5, Staff=7, Bow=8, Crossbow=9
    }
}
