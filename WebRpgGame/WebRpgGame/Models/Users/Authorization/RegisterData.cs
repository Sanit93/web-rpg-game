﻿using System.ComponentModel.DataAnnotations;
using WebRpgGame.Models.Users;

namespace WebRpgGame.Models
{
    public class RegisterData
    {
        [Required]
        public AuthorizationData AuthorizationData { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
