﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebRpgGame.Models.Equipment;
using WebRpgGame.Models.Items;
using WebRpgGame.Models.Items.Weapons;
using WebRpgGame.Models.MongoDb;

namespace WebRpgGame.Models
{
    public class Character
    {
        [BsonId]
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }

        public byte[] SHA1 { get; set; }

        public int Strenght { get; set; }
        public int Agility { get; set; }
        public int Inteligence { get; set; }
        public int Endurance { get; set; }
        public int Luck { get; set; }

        public int Damage { get; set; }
        public int Armor { get; set; }

        Item[] Equipment { get; set; }
        List<Item> Items { get; set; }

        public Character()
        {
            Damage = 10;
            Armor = 0;
            
            Strenght = 0;
            Agility = 0;
            Inteligence = 0;
            Endurance = 0;
            Luck = 0;

            Equipment = new Item[8];
            Items = new List<Item>();
        }

        public void IncraseStat(int stat)
        {
            Character newChar = this;
            switch (stat)
            {
                case 0:
                    newChar.Strenght++;
                    break;
                case 1:
                    newChar.Agility++;
                    break;
                case 2:
                    newChar.Inteligence++;
                    break;
                case 3:
                    newChar.Endurance++;
                    break;
                case 4:
                    newChar.Luck++;
                    break;
            }
            Repository.UpdateRecord("Users", Id, newChar);
        }

        public void EquipWeapon(Weapon weapon)
        {
            var slot = Convert.ToInt16(weapon.Slot);

            {
                Damage = Strenght;
                Equipment[slot] = weapon;
                Character newChar = this;
                newChar.Damage += weapon.Damage;
                Repository.UpdateRecord("Users", Id, newChar);
            }
            
        }
    }
}
